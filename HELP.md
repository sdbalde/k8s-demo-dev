# Getting Started
###Minikube commands
 * minikube start (to start cluster)
###Skaffold commands
 * skaffold run --por-forward (to run application : build and deploy)
 * skaffold dev (to run application in dev mode : build and deploy and watch file changes to re-run)
 * skaffold delete (to undeploy application)
### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.4/maven-plugin/reference/html/#build-image)

