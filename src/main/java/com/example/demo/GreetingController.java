package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    private static final String template = "Hello Kubernetes, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name",
            defaultValue="World") String name) {

        Greeting result = new Greeting(counter.incrementAndGet(),
                String.format(template, name));
        return result;
    }
    @GetMapping("/")
    public Object welcome() {
        return "Welcome Page";
    }


    public class Greeting {

        private final long id;
        private final String content;

        public Greeting(long id, String content) {
            this.id = id;
            this.content = content;
        }

        public long getId() {
            return id;
        }

        public String getContent() {
            return content;
        }

        @Override
        public String toString() {
            return String.format("{id:%s,content:%s}",id, content);
        }
    }
}
